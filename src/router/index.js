import Vue from 'vue';
import Router from 'vue-router';
import Soundboard from '@/components/Soundboard';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Soundboard',
      component: Soundboard,
    },
  ],
});
